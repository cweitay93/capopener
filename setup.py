import setuptools

with open('README.md', 'r') as f:
    long_desc = f.read()

setuptools.setup(
    name = "capopener-soveran",
    version = "0.0.1",
    author = "Soveran",
    description = "A random python package to dissect a pcap file",
    long_description = long_desc,
    long_description_content_type = "text/markdown",
    url = "-",
    packages = setuptools.find_packages(),
    include_package_data = True,
    install_requires = [
        "scapy", "argparse"
    ],
    classifiers = [
        "Programming Language :: Python :: 3",
        "License :: MIT LICENSE",
        "Operating System :: OS Independent"
    ],
    python_requires = ">=3.6",
    entry_points = {
        "console_scripts": ["capopener=capopener.__main__:main"]
    },
)