# Practice Python CI/CD

This project is created to learn how to build a CI/CD process to test,
package and deploy a python application in a virtual production environment.
The application uses scapy library to dissect a PCAP file.