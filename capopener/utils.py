# utils.py

from scapy.all import PcapWriter

class Utils:
    @staticmethod
    def write_new_pcap(loc):
        try:
            writer=PcapWriter(loc)
            return 1
        except:
            return 0