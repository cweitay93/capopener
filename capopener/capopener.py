# capopener.py
import os

from .utils import Utils as utils

class CapOpener:
    @staticmethod
    def display_pcap_file(pcap_file):
        print('{}'.format(pcap_file))

    @staticmethod
    def create_pcap_file(pcap_file, loc):
        pcap_loc = os.path.join("{}/{}".format(loc,pcap_file))
        if utils.write_new_pcap(pcap_loc):
            print('PCAP file created')
        else:
            print('Failed to create PCAP file.')
