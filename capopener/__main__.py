# __main__.py
import os
import sys
import argparse

from .capopener import CapOpener as co

class CustomParser(argparse.ArgumentParser):
    def error(self, msg):
        sys.stderr.write('Error: {}\n'.format(msg))
        self.print_help()
        sys.exit(2)

def main():
    parser = CustomParser(description = 'Time to dissect some PCAP files!')
    parser.add_argument('-f', '--pcap-file', 
                        metavar='PCAP_FILE', 
                        type=str,
                        dest='pf',
                        help='The pcap file to dissect.')
    parser.add_argument('-C', '--create',
                        metavar='NEW_PCAP',
                        type=str,
                        dest='pc',
                        help='Create PCAP file')

    if len(sys.argv) > 1:
        args = parser.parse_args()
        if args.pc is not None:
            co.create_pcap_file(args.pc, os.path.join(os.getcwd(), 'output/'))
        else:
            co.display_pcap_file(args.pf)
    else:
        parser.print_help()
    return 0