import os
import pytest

from capopener.utils import Utils

def test_write_new_pcap(tmp_path):
    pcap_file = os.path.join(tmp_path, "test_write.pcap")
    res = Utils.write_new_pcap(pcap_file)
    
    assert res == 1
    assert os.path.exists(pcap_file) == True