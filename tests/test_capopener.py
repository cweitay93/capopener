# test_capopener.py
import os
import pytest

from capopener.capopener import CapOpener

def test_display_pcap_file(capsys):
    CapOpener.display_pcap_file("test.pcap")
    print_res = capsys.readouterr()

    assert print_res.out == "test.pcap\n"

def test_create_pcap_file(tmp_path, capsys):
    CapOpener.create_pcap_file("test.pcap", tmp_path)
    res = os.path.exists(os.path.join(tmp_path, "test.pcap"))
    print_res = capsys.readouterr()

    assert res == True
    assert print_res.out == "PCAP file created\n"